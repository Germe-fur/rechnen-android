/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.rechnen.app.R

class EnableOperatorView(context: Context, attributeSet: AttributeSet): FrameLayout(context, attributeSet) {
    init {
        inflate(context, R.layout.enable_operator_view, this)
    }

    private val enableSwitch = findViewById<SwitchCompat>(R.id.enable_operator_switch)
    private val cannotDisableMessage = findViewById<TextView>(R.id.cannot_disable_last_operator_message)
    var listener: EnableOperatorListener? = null

    var status: EnableOperatorStatus = EnableOperatorStatus.Disabled
        set(value) {
            field = value

            enableSwitch.isChecked = value != EnableOperatorStatus.Disabled
            enableSwitch.isEnabled = value != EnableOperatorStatus.LastEnabled
            cannotDisableMessage.visibility = if (value == EnableOperatorStatus.LastEnabled) View.VISIBLE else View.GONE
        }

    init {
        enableSwitch.setOnCheckedChangeListener { _, isChecked ->
            val shouldBeChecked = status != EnableOperatorStatus.Disabled

            if (isChecked != shouldBeChecked) {
                listener?.updateEnableOperator(isChecked)
            }
        }
    }
}

enum class EnableOperatorStatus {
    Disabled, Enabled, LastEnabled
}

interface EnableOperatorListener {
    fun updateEnableOperator(enable: Boolean)
}