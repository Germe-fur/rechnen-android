/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.userlist

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rechnen.app.R
import com.rechnen.app.data.model.User
import com.rechnen.app.databinding.UserListItemBinding
import com.rechnen.app.ui.util.Time
import kotlin.properties.Delegates

class UserListAdapter: RecyclerView.Adapter<UserListViewHolder>() {
    var data: List<User> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }
    var listener: UserListAdapterListener? = null

    init {
        setHasStableIds(true)
    }

    override fun getItemCount(): Int = data.size
    override fun getItemId(position: Int): Long = data[position].id.toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListViewHolder = UserListViewHolder(
            UserListItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
            )
    )

    override fun onBindViewHolder(holder: UserListViewHolder, position: Int) {
        val binding = holder.binding
        val context = binding.root.context
        val item = data[position]

        binding.username = item.name

        if (item.lastUse != null) {
            binding.lastUsageDate = DateUtils.formatDateTime(context, item.lastUse, DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_SHOW_TIME)
            binding.lastUsageBlockCount = context.resources.getQuantityString(R.plurals.user_list_blocks, item.lastBlockCount, item.lastBlockCount)
        } else {
            binding.lastUsageDate = null
            binding.lastUsageBlockCount = null
        }

        binding.playDuration = if (item.playTime == 0L) null else Time.formatTime(item.playTime, context)

        binding.root.setOnClickListener { listener?.onItemClicked(item) }
        binding.root.setOnLongClickListener { listener?.onItemLongClicked(item) ?: false }

        binding.executePendingBindings()
    }
}

interface UserListAdapterListener {
    fun onItemClicked(user: User)
    fun onItemLongClicked(user: User): Boolean
}

class UserListViewHolder(val binding: UserListItemBinding): RecyclerView.ViewHolder(binding.root)