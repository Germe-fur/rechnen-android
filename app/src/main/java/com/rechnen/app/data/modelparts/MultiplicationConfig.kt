/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

data class MultiplicationConfig(
        val enable: Boolean = true,
        val digitsOfFirstNumber: Int = 1,
        val digitsOfSecondNumber: Int = 1
) {
    companion object {
        private const val ENABLE = "enable"
        private const val DIGITS_OF_FIRST_NUMBER = "digitsOfFirstNumber"
        private const val DIGITS_OF_SECOND_NUMBER = "digitsOfSecondNumber"

        fun parse(reader: JsonReader): MultiplicationConfig {
            var enable: Boolean? = null
            var digitsOfFirstNumber: Int? = null
            var digitsOfSecondNumber: Int? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ENABLE -> enable = reader.nextBoolean()
                    DIGITS_OF_FIRST_NUMBER -> digitsOfFirstNumber = reader.nextInt()
                    DIGITS_OF_SECOND_NUMBER -> digitsOfSecondNumber = reader.nextInt()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return MultiplicationConfig(
                    enable = enable!!,
                    digitsOfFirstNumber = digitsOfFirstNumber!!,
                    digitsOfSecondNumber = digitsOfSecondNumber!!
            )
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ENABLE).value(enable)
        writer.name(DIGITS_OF_FIRST_NUMBER).value(digitsOfFirstNumber)
        writer.name(DIGITS_OF_SECOND_NUMBER).value(digitsOfSecondNumber)

        writer.endObject()
    }
}