/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rechnen.app.data.modelparts.TaskDifficulty
import com.rechnen.app.data.model.User
import com.rechnen.app.data.modelparts.InputConfiguration

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    fun getAllUsers(): LiveData<List<User>>

    @Query("SELECT * FROM user WHERE id = :userId")
    suspend fun getUserByIdCoroutine(userId: Int): User?

    @Query("SELECT * FROM user WHERE id = :userId")
    fun getUserByIdLive(userId: Int): LiveData<User?>

    @Query("SELECT solved_tasks FROM user WHERE id = :userId")
    fun getUserSolvedTasksSync(userId: Int): Long?

    @Insert
    fun addUserSync(user: User)

    @Query("DELETE FROM user WHERE id = :userId")
    fun deleteUserByIdSync(userId: Int)

    @Query("UPDATE user SET last_use = :timestamp, last_block_count = :blockCount WHERE id = :userId")
    suspend fun updateUserLastUseAndBlockCountCoroutine(userId: Int, timestamp: Long, blockCount: Int)

    @Query("UPDATE user SET difficulty = :difficulty WHERE id = :userId")
    fun updateUserDifficultyConfigSync(userId: Int, difficulty: TaskDifficulty)

    @Query("UPDATE user SET input_configuration = :inputConfiguration WHERE id = :userId")
    fun updateUserInputConfigurationSync(userId: Int, inputConfiguration: InputConfiguration)

    @Query("UPDATE user SET play_time = play_time + :timeToAdd WHERE id = :userId")
    fun increaseUserPlayTimeSync(userId: Int, timeToAdd: Long)

    @Query("UPDATE user SET solved_tasks = :solvedTasks WHERE id = :userId")
    fun updateUserSolvedTasksSync(userId: Int, solvedTasks: Long)

    @Query("SELECT difficulty FROM user WHERE id = :userId")
    fun getUserDifficultyByUserIdSync(userId: Int): TaskDifficulty?
}