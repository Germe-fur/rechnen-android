/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

data class InputConfiguration(
        val type: KeyboardType,
        val size: Int,
        val horizontal: HorizontalAlignment,
        val vertical: VerticalAlignment,
        val confirmButtonLocation: ConfirmButtonLocation
) {
    companion object {
        private const val TYPE = "type"
        private const val SIZE = "size"
        private const val HORIZONTAL = "horizontal"
        private const val VERTICAL = "vertical"
        private const val CONFIRM_BUTTON_LOCATION = "confirmButtonLocation"

        val default = InputConfiguration(
                type = KeyboardType.Phone,
                size = 0,
                horizontal = HorizontalAlignment.Center,
                vertical = VerticalAlignment.Center,
                confirmButtonLocation = ConfirmButtonLocation.Right
        )

        fun parse(reader: JsonReader): InputConfiguration {
            var type: KeyboardType = default.type
            var size: Int = default.size
            var horizontal: HorizontalAlignment = default.horizontal
            var vertical: VerticalAlignment = default.vertical
            var confirmButtonLocation: ConfirmButtonLocation = default.confirmButtonLocation

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    TYPE -> type = KeyboardTypes.fromString(reader.nextString())
                    SIZE -> size = reader.nextInt()
                    HORIZONTAL -> horizontal = HorizontalAlignments.fromString(reader.nextString())
                    VERTICAL -> vertical = VerticalAlignments.fromString(reader.nextString())
                    CONFIRM_BUTTON_LOCATION -> confirmButtonLocation = ConfirmButtonLocations.fromString(reader.nextString())
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return InputConfiguration(
                    type = type,
                    size = size,
                    horizontal = horizontal,
                    vertical = vertical,
                    confirmButtonLocation = confirmButtonLocation
            )
        }
    }

    enum class KeyboardType {
        Phone, Calculator
    }

    object KeyboardTypes {
        private const val PHONE = "phone"
        private const val CALCULATOR = "calculator"

        fun fromString(type: String): KeyboardType = when (type) {
            PHONE -> KeyboardType.Phone
            CALCULATOR -> KeyboardType.Calculator
            else -> throw IllegalArgumentException()
        }

        fun toString(type: KeyboardType): String = when (type) {
            KeyboardType.Phone -> PHONE
            KeyboardType.Calculator -> CALCULATOR
        }
    }

    enum class HorizontalAlignment {
        Left, Center, Right
    }

    object HorizontalAlignments {
        private const val LEFT = "left"
        private const val CENTER = "center"
        private const val RIGHT = "right"

        fun fromString(type: String): HorizontalAlignment = when (type) {
            LEFT -> HorizontalAlignment.Left
            CENTER -> HorizontalAlignment.Center
            RIGHT -> HorizontalAlignment.Right
            else -> throw IllegalArgumentException()
        }

        fun toString(type: HorizontalAlignment): String = when (type) {
            HorizontalAlignment.Left -> LEFT
            HorizontalAlignment.Center -> CENTER
            HorizontalAlignment.Right -> RIGHT
        }
    }

    enum class VerticalAlignment {
        Top, Center, Bottom
    }

    object VerticalAlignments {
        private const val TOP = "top"
        private const val CENTER = "center"
        private const val BOTTOM = "bottom"

        fun fromString(type: String): VerticalAlignment = when(type) {
            TOP -> VerticalAlignment.Top
            CENTER -> VerticalAlignment.Center
            BOTTOM -> VerticalAlignment.Bottom
            else -> throw IllegalArgumentException()
        }

        fun toString(type: VerticalAlignment): String = when (type) {
            VerticalAlignment.Top -> TOP
            VerticalAlignment.Center -> CENTER
            VerticalAlignment.Bottom -> BOTTOM
        }
    }

    enum class ConfirmButtonLocation {
        Left, Right
    }

    object ConfirmButtonLocations {
        private const val LEFT = "left"
        private const val RIGHT = "right"

        fun fromString(type: String): ConfirmButtonLocation = when (type) {
            LEFT -> ConfirmButtonLocation.Left
            RIGHT -> ConfirmButtonLocation.Right
            else -> throw IllegalArgumentException()
        }

        fun toString(type: ConfirmButtonLocation): String = when (type) {
            ConfirmButtonLocation.Left -> LEFT
            ConfirmButtonLocation.Right -> RIGHT
        }
    }

    init {
        if (size < 0 || size > 100) throw IllegalArgumentException()
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(TYPE).value(KeyboardTypes.toString(type))
        writer.name(SIZE).value(size)
        writer.name(HORIZONTAL).value(HorizontalAlignments.toString(horizontal))
        writer.name(VERTICAL).value(VerticalAlignments.toString(vertical))
        writer.name(CONFIRM_BUTTON_LOCATION).value(ConfirmButtonLocations.toString(confirmButtonLocation))

        writer.endObject()
    }
}